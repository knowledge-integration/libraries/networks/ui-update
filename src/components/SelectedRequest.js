import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Headline, KeyValue } from '@folio/stripes/components';
import { useOkapiQuery } from '@k-int/stripes-ill';
import { CatalogInfo, RequesterSupplier } from '@k-int/stripes-ill/cards';
import { useStripes } from '@folio/stripes/core';
import { PATRON_REQUESTS_ENDPOINT } from '@k-int/ill-ui';

const SelectedRequest = ({ initialRequest, initialRequestTime }) => {
  const stripes = useStripes();
  const q = useOkapiQuery(`${PATRON_REQUESTS_ENDPOINT}/${initialRequest.id}`, {
    initialData: initialRequest,
    initialDataUpdatedAt: initialRequestTime,
    staleTime: 2 * 60 * 1000,
  });
  const request = q.data;

  return (
    <>
      <KeyValue label={<FormattedMessage id="stripes-ill.requestState" />}>
        <Headline size="large" faded><FormattedMessage id={`stripes-ill.states.${request.state?.code}`} /></Headline>
      </KeyValue>
      <CatalogInfo request={request} stripes={stripes} />
      <RequesterSupplier request={request} />
    </>
  );
};

export default SelectedRequest;
