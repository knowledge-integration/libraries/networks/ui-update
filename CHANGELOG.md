## [1.7.2](https://gitlab.com/knowledge-integration/libraries/networks/ill-update/compare/v1.7.1...v1.7.2) (2023-11-02)

## [1.7.1](https://gitlab.com/knowledge-integration/libraries/networks/ill-update/compare/v1.7.0...v1.7.1) (2023-10-21)


### Bug Fixes

* **build:** Add .releaserc file and script settings ([634c13f](https://gitlab.com/knowledge-integration/libraries/networks/ill-update/commit/634c13f969f4596b5c3dd5d5fbfb303e92559fb5))
* **build:** Updated CI script to publish Module Descriptor ([1c828ff](https://gitlab.com/knowledge-integration/libraries/networks/ill-update/commit/1c828ffd5a02bc125292af6d0ccf233a941e241c))

# Change history for ui-update

## [1.0.0](https://github.com/openlibraryenvironment/ui-update/tree/v1.0.0) (2020-08-24)

* New app created with stripes-cli
* Initial release
